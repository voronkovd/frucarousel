# -*- coding: utf-8 -*-
from django.db.models import PositiveIntegerField

from app.models import AbstractAppModel


class Slider(AbstractAppModel):
    order = PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'sliders'
        verbose_name = u'Изображение в слайдер'
        verbose_name_plural = u'Изображения слайдера'
        ordering = ['order']
