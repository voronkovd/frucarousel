# -*- coding: utf-8 -*-
from django.template import Library

from slider.models import Slider

register = Library()


@register.inclusion_tag('slider/list.html')
def slider_view(image_only=True):
    sliders = Slider.objects.filter(published=True).all()
    return {'sliders': sliders, 'image_only': image_only}
