(function () {

    jQuery(window).load(function () {
        jQuery('.all-wrapper').css({'opacity': 1, 'visibility': 'visible'});
        jQuery('.back-to-top').css({'opacity': 1, 'visibility': 'visible'});
    });
    jQuery(document).ready(function () {

        //Init the carousel
        jQuery(".slider").owlCarousel({
            animateIn: 'owl-fadeUp-in',
            animateOut: 'owl-fadeUp-out',
            items: 3,
            margin: 0,
            loop: true,
            autoplay: true,
            autoplayTimeout: 6000,
            autoplayHoverPause: false,
            nav: true,
            dots: true,
            stagePadding: 0,
            smartSpeed: 2500,
            mouseDrag: true,
            touchDrag: true,
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1
                }
            }
        });

    });
    jQuery(document).ready(function () {

        var owl = jQuery(".carousel-col-2");

        owl.owlCarousel({
            nav: true,
            dots: true,
            loop: true,
            autoplay: true,
            autoplayHoverPause: true,
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1
                },
                768: {
                    items: 1,
                    slideBy: 1
                },
                1000: {
                    items: 2,
                    slideBy: 2
                }
            }
        });
    });
    jQuery(document).ready(function () {

        var owl = jQuery(".carousel-col-3");

        owl.owlCarousel({
            nav: true,
            dots: false,
            loop: true,
            autoplay: false,
            autoplayHoverPause: true,
            items: 3,
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1
                },
                992: {
                    items: 2,
                    slideBy: 2
                },
                1200: {
                    items: 3,
                    slideBy: 3
                }
            }
        });
    });
    jQuery(document).ready(function () {

        var owl = jQuery(".carousel-col-4");

        owl.owlCarousel({
            nav: true,
            dots: true,
            loop: true,
            autoplay: true,
            autoplayHoverPause: true,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1
                },
                768: {
                    items: 2,
                    slideBy: 2
                },
                992: {
                    items: 3,
                    slideBy: 3
                },
                1200: {
                    items: 4,
                    slideBy: 4
                }
            }
        });
    });
    jQuery(document).ready(function () {
        $('.pretty-social').prettySocial();
    });

    jQuery(document).ready(function () {
        var duration = 1500;

        jQuery('.back-to-top').click(function (e) {
            e.stopPropagation();
            jQuery('body,html').animate({
                scrollTop: 0
            }, duration);
            return false;
        })
    });


})();