SirTrevor.Locales.ru = {
  general: {
    'delete':           'Удалить?',
    'drop':             '__block__ Перетащите сюда',
    'paste':            'Или вставить адрес сюда',
    'upload':           '...oder Datei auswählen',
    'close':            'Schließen',
    'position':         'Position',
    'wait':             'Bitte warten...',
    'link':             'Link eintragen'
  },
  errors: {
    'title': "Die folgenden Fehler sind aufgetreten:",
    'validation_fail': "Block __type__ ist ungültig",
    'block_empty': "__name__ darf nicht leer sein",
    'type_missing': "Blöcke mit Typ __type__ sind hier nicht zulässig",
    'required_type_empty': "Angeforderter Block-Typ __type__ ist leer",
    'load_fail': "Es wurde ein Problem beim Laden des Dokumentinhalts festgestellt"
  },
  blocks: {
    text: {
      'title': "Текст"
    },
    list: {
      'title': "Liste (unsortiert)"
    },
    quote: {
      'title': "Zitat",
      'credit_field': "Quelle"
    },
    image: {
      'title': "Bild",
      'upload_error': "There was a problem with your upload"
    },
    video: {
      'title': "Видео"
    },
    tweet: {
      'title': "Твит",
      'fetch_error': "Es wurde ein Problem beim Laden des Tweets festgestellt"
    },
    embedly: {
      'title': "Embedly",
      'fetch_error': "There was a problem fetching your embed",
      'key_missing': "An Embedly API key must be present"
    },
    heading: {
      'title': 'Заголовок'
    }
  }
};