# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from views import about, contact, delivery

urlpatterns = patterns('',
                       url(r'^about/$', about, name='page_about'),
                       url(r'^contact/$', contact, name='page_contact'),
                       url(r'^delivery/$', delivery, name='page_delivery'),
                       )
