# -*- coding: utf-8 -*-
from django.views import generic


class About(generic.TemplateView):
    template_name = 'page/about.html'


class Contact(generic.TemplateView):
    template_name = 'page/contact.html'


class Delivery(generic.TemplateView):
    template_name = 'page/delivery.html'


about = About.as_view()
contact = Contact.as_view()
delivery = Delivery.as_view()
