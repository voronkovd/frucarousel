# -*- coding: utf-8 -*-
from django.views import generic

from models import Blog


class BlogList(generic.ListView):
    model = Blog
    paginate_by = 6
    queryset = Blog.objects.filter(published=True).all()
    template_name = 'blog/list.html'


class BlogDetail(generic.DetailView):
    model = Blog
    template_name = 'blog/detail.html'

    def get_queryset(self):
        queryset = super(BlogDetail, self).get_queryset()
        return queryset.filter(published=True)


blog_list = BlogList.as_view()
blog_detail = BlogDetail.as_view()
