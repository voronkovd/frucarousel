# -*- coding: utf-8 -*-
from django.db.models import TextField
from image_cropping import ImageRatioField

from app.models import AbstractAppModel


class Blog(AbstractAppModel):
    anons = TextField(max_length=1000, verbose_name=u'Анонс')
    cropping = ImageRatioField('image', '368x368', verbose_name=u'Пропорции')

    class Meta:
        db_table = 'blogs'
        verbose_name = u'Статью'
        verbose_name_plural = u'Статьи'
        ordering = ['-created']

    def get_absolute_url(self):
        return '/blog/%s' % str(self.pk)
