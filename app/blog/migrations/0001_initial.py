# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields
import app.utils
import sirtrevor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('content', sirtrevor.fields.SirTrevorField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('image', models.ImageField(upload_to=app.utils.upload_to, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('created', models.DateField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('published', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('anons', models.TextField(max_length=1000, verbose_name='\u0410\u043d\u043e\u043d\u0441')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '368x368', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438')),
            ],
            options={
                'ordering': ['-created'],
                'db_table': 'blogs',
                'verbose_name': '\u0421\u0442\u0430\u0442\u044c\u044e',
                'verbose_name_plural': '\u0421\u0442\u0430\u0442\u044c\u0438',
            },
        ),
    ]
