# -*- coding: utf-8 -*-
from django.views import generic

from work.models import Work


class Index(generic.TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(Index, self).get_context_data(**kwargs)
        ctx['works'] = Work.objects.filter(published=True,
                                           index_page=True).all()[:4]
        return ctx


index = Index.as_view()
