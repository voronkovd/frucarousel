# -*- coding: utf-8 -*-
import os
import json

from django.conf import settings
from django.db import models
from easy_thumbnails.files import get_thumbnailer
from sirtrevor.fields import SirTrevorField

from utils import upload_to as to


class AbstractAppModel(models.Model):
    title = models.CharField(max_length=128, verbose_name=u'Заголовок')
    content = SirTrevorField(verbose_name=u'Текст')
    image = models.ImageField(upload_to=to, verbose_name=u'Изображение')
    created = models.DateField(editable=False, auto_now_add=True,
                               verbose_name=u'Дата добавления')
    published = models.BooleanField(default=False, verbose_name=u'Отображать')

    class Meta:
        abstract = True

    def __unicode__(self):
        return unicode(self.title)

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Изображение'

    image_tag.allow_tags = True

    def content_to_text(self):
        content = json.loads(self.content)
        string = ''
        for block in content['data']:
            string += (block['data']['text']).replace('\\', '')
        return string
