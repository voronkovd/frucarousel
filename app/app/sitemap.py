# -*- coding: utf-8 -*-
from blog.models import Blog
from work.models import Work

blog_dict = {'queryset': Blog.objects.filter(published=True).all()}
work_dict = {'queryset': Work.objects.filter(published=True).all()}
