# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from views import work_detail, work_list

urlpatterns = patterns('',
                       url(r'^index/$', work_list, name='work_list'),
                       url(r'^(?P<pk>[\w-]+)$', work_detail,
                           name='work_detail'),
                       )
