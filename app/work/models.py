# -*- coding: utf-8 -*-
from django.db import models
from image_cropping import ImageRatioField

from app.models import AbstractAppModel


class Work(AbstractAppModel):
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)
    cropping = ImageRatioField('image', '268x268', verbose_name=u'Пропорции')
    index_page = models.BooleanField(default=False, verbose_name=u'Га главной')

    class Meta:
        db_table = 'works'
        verbose_name = u'Работу'
        verbose_name_plural = u'Работы'
        ordering = ['order']

    def gallery(self):
        return self.workimage_set.all()

    def get_absolute_url(self):
        return '/work/%s' % str(self.pk)
