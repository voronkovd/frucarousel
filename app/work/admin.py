# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from suit.admin import SortableModelAdmin
from image_cropping import ImageCroppingMixin

from models import Work


class WorkAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    list_display = ('title', 'image_tag', 'published')
    list_filter = ('published',)
    sortable = 'order'


admin.site.register(Work, WorkAdmin)
