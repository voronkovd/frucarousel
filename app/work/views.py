# -*- coding: utf-8 -*-
from django.views import generic

from models import Work


class WorkList(generic.ListView):
    model = Work
    paginate_by = 4
    queryset = Work.objects.filter(published=True).all()
    template_name = 'work/list.html'


class WorkDetail(generic.DetailView):
    model = Work
    template_name = 'work/detail.html'

    def get_queryset(self):
        queryset = super(WorkDetail, self).get_queryset()
        return queryset.filter(published=True)


work_list = WorkList.as_view()
work_detail = WorkDetail.as_view()
