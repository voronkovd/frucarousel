# frucarousel.ru

Installation:

```sh
$ git clone [git-repo-url]
$ pip install virtualenv
$ virtaulenv env
$ source env/bin/activate
$ pip install -r requirements.txt
$ cd app/
$ cp app/settings.default.ini app/settings.ini
$ vim app/settings.conf
change your config file
$ python manage.py migrate
$ python manage.py runserver
```
